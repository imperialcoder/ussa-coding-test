const KoaRouter = require('koa-router');
const serve = require('koa-static');

const indexRouter = new KoaRouter();

indexRouter.get('/', serve('public/index.html'));

module.exports = indexRouter;
