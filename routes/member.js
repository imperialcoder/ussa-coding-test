const KoaRouter = require('koa-router');
const sqlite3 = require('sqlite3').verbose();
const { koaBody } = require('koa-body');

const memberRouter = new KoaRouter({ prefix: '/member' });

let db = new sqlite3.Database(
    'data/sqlitedb',
    sqlite3.OPEN_READWRITE,
    (error) => {
        if (error) console.error(error);
    }
);

memberRouter.post('/', koaBody(), async function (ctx) {
    const pageIndex = ctx.request.body?.pageIndex;
    const searchString = ctx.request.body?.searchString;

    await new Promise((resolve, reject) => {
        db.all(
            `SELECT *, count(*) OVER() AS full_count
             FROM members 
             INNER JOIN addresses
             ON members.id = addresses.memberId
             WHERE firstName LIKE ? 
             OR lastName LIKE ?
             LIMIT 100 OFFSET ?`,
            ['%' + searchString + '%', '%' + searchString + '%', pageIndex],
            (error, rows) => {
                if (error) reject(error);
                ctx.body = rows;
                resolve();
            }
        );
    }).catch((error) => {
        console.log(error);
    });
});

module.exports = memberRouter;
