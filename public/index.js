let paginatorRadioInputs;
const memberList = document.querySelector('ul.member-list');
const searchInput = document.querySelector(
    "input[name='search'].search-bar__input"
);
const searchButton = document.querySelector('button.search-bar__button');
const paginatorForm = document.querySelector('form.paginator__form');
const paginatorNextButton = document.querySelector('button.paginator__next');
const paginatorPrevButton = document.querySelector('button.paginator__prev');

async function getMemberData(pageIndex, searchString) {
    const url = '/member';
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            pageIndex,
            searchString,
        }),
    };

    return await fetch(url, options)
        .then((response) => {
            if (response.ok && response.status == 200) {
                return response.json();
            } else {
                throw new Error(response);
            }
        })
        .catch((error) => {
            console.log(error);
        });
}

function insertMemberListItem(left_fields, right_fields, image_url) {
    const mapFields = (fields) =>
        fields.map(
            (field) => `
                <h4 class="member-list__title">
                    ${field.title}
                </h4>
                <p class="member-list__description">
                    ${field.description}
                </p>`
        );

    memberList.innerHTML += `
        <li class="member-list__item">
            <div class="member-list__column">
                ${mapFields(left_fields).join('')}
            </div>
            <div class="member-list__column">
                ${mapFields(right_fields).join('')}
            </div>
            <img class="member-list__image" src="${image_url}" />
        </li>`;
}

function searchSubmit(pageIndex, searchString) {
    memberList.innerHTML = '';

    getMemberData(pageIndex, searchString).then((members_array) => {
        setRadioInputs(
            Math.round(members_array[0]?.full_count / 100),
            pageIndex
        );

        members_array.forEach((member) => {
            const address_field = {
                title: ' Address:',
                description: [
                    member?.address,
                    member?.city,
                    member?.state,
                    member?.zip,
                ].join(', '),
            };
            insertMemberListItem(
                [
                    {
                        title: ' Name:',
                        description: `${member.firstName} ${member.lastName}`,
                    },
                    { title: ' Company:', description: member.company },
                    {
                        title: '󰴹 Department:',
                        description: member.department,
                    },
                ],
                [
                    { title: ' Phone:', description: member.phone },
                    ...(member.primaryAddress === 1 ? [address_field] : []),
                    { title: '󰖟 Website:', description: member.url },
                ],
                member.image
            );
        });
    });
}

function searchEventHandler(event) {
    switch (event.target.className) {
        case 'search-bar__input':
            if (event.key == 'Enter') {
                searchSubmit(0, event.target.value);
            }
            break;
        case 'search-bar__button':
            event.preventDefault();
            searchSubmit(0, searchInput.value);
            break;
    }
}

function insertPaginatorRadioInput(index) {
    paginatorForm.innerHTML += `
        <label class="paginator__label">
            <input
                id="paginator__input--${index}"
                class="paginator__input"
                type="radio"
                name="paginator"
                value="${index}"
            />
            <span class="paginator__span">${index}</span>
        </label>`;
}

function setRadioInputs(pageLength, pageIndex) {
    paginatorForm.innerHTML = '';
    for (i = 0; i < pageLength; i++) {
        insertPaginatorRadioInput(i + 1);
    }
    paginatorRadioInputs = document.querySelectorAll(
        "input[name='paginator'].paginator__input"
    );

    paginatorRadioInputs.forEach((radio, index) => {
        if (index === pageIndex) radio.checked = true;
        radio.addEventListener('change', radioEventHandler);
    });
}

function getPageIndex() {
    for (index = 0; index < paginatorRadioInputs.length; index++) {
        if (paginatorRadioInputs[index].checked) {
            return index;
        }
    }
}

function radioEventHandler(event) {
    event.preventDefault;

    const className = event.target.className;
    const pageIndex = getPageIndex();
    const pageLength = paginatorRadioInputs.length;

    switch (className) {
        case 'paginator__input':
            searchSubmit(pageIndex, searchInput.value);
            break;
        case 'paginator__next':
            if (pageIndex < pageLength - 1) {
                searchSubmit(pageIndex + 1, searchInput.value);
            }
            break;
        case 'paginator__prev':
            if (pageIndex >= 1) {
                searchSubmit(pageIndex - 1, searchInput.value);
            }
            break;
    }
}

searchInput.addEventListener('keypress', searchEventHandler);
searchButton.addEventListener('click', searchEventHandler);
paginatorNextButton.addEventListener('click', radioEventHandler);
paginatorPrevButton.addEventListener('click', radioEventHandler);

searchInput.value = '';
searchSubmit(0, '');
