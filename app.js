if (require.main === module) {
    throw new Error('Do not run directly. Use server.js to start.');
}

const Koa = require('koa');
const koaStatic = require('koa-static');

const app = new Koa();

// Register Routes
let indexRouter = require('./routes/index');
let memberRouter = require('./routes/member');

app.use(indexRouter.routes()).use(indexRouter.allowedMethods());
app.use(memberRouter.routes()).use(memberRouter.allowedMethods());

// Serve static files from public directory
app.use(koaStatic('./public'));

module.exports = app;
